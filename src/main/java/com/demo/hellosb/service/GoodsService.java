package com.demo.hellosb.service;


import com.demo.hellosb.common.domain.Goods;
import com.github.pagehelper.PageInfo;

public interface GoodsService {
    PageInfo<Goods> findGoods(int pageNo, int pageSize);

    void deleteGoodsById(String... id);

    Goods findGoodsById(String id);

    int goodsinset(Goods goods);

    int goodsupdate(Goods goods);
}
