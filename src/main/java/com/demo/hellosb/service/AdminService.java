package com.demo.hellosb.service;

import com.demo.hellosb.common.domain.Admin;

public interface AdminService {
    public Admin findAdminByLoginname(String name);
    public int insertAdmin(Admin admin);
}
