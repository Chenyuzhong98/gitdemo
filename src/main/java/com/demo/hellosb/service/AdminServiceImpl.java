package com.demo.hellosb.service;

import com.demo.hellosb.common.domain.Admin;
import com.demo.hellosb.common.domain.AdminExample;
import com.demo.hellosb.mapper.AdminMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminServiceImpl implements AdminService {

    @Autowired
    AdminMapper adminMapper;//服务层 注入  dao 层
    public Admin findAdminByLoginname(String name) {
        AdminExample example = new AdminExample();
        example.createCriteria().andLoginnameEqualTo(name);
        List<Admin> admins = adminMapper.selectByExample(example);
        return admins.size()>0?admins.get(0):null;
    }

    @Override
    public int insertAdmin(Admin admin) {
       return adminMapper.insert(admin);

    }
}
