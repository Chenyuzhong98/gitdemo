package com.demo.hellosb.service;


import com.demo.hellosb.common.domain.Goods;
import com.demo.hellosb.common.domain.GoodsExample;
import com.demo.hellosb.mapper.GoodsMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

@Service
public class GoodsServiceImpl implements GoodsService{
    @Autowired
    GoodsMapper goodsMapper;
    @Override
    public PageInfo<Goods> findGoods(int pageNo, int pageSize) {
        PageHelper.startPage(pageNo,pageSize);
        List<Goods> goods = goodsMapper.selectByExample(null);
        return new PageInfo<>(goods);
    }

    @Override
    public void deleteGoodsById(String... id) {
        GoodsExample  goodsExample= new GoodsExample();
        goodsExample.createCriteria().andIdIn(Arrays.asList(id));
        goodsMapper.deleteByExample(goodsExample);
    }

    @Override
    public Goods findGoodsById(String id) {
        return goodsMapper.selectByPrimaryKey(id);
    }

    @Override
    public int goodsinset(Goods goods) {
        return goodsMapper.insert(goods);
    }

    @Override
    public int goodsupdate(Goods goods) {
        return goodsMapper.updateByPrimaryKey(goods);
    }
}
