package com.demo.hellosb.controller;

import com.demo.hellosb.common.domain.Admin;
import com.demo.hellosb.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.UUID;

@Controller
public class AdminController {
    //
    @Autowired
    private AdminService adminService;

    @RequestMapping("/admin/login")
    public String login(ModelMap modelMap, Admin admin, HttpServletRequest request) {
        Admin adminDB = adminService.findAdminByLoginname(admin.getLoginname());
        boolean success = true;
        String failMsg = null;
        if (adminDB == null) {
            success = false;
            failMsg = "用户名不存在";
        } else if (!(adminDB.getPassword().equals(admin.getPassword()))) {
            success = false;
            failMsg = "密码不匹配";
        }
        modelMap.put("content", "main");
        modelMap.put("fragment","content2");
        modelMap.put("failMsg", failMsg);
        if(success){
            request.getSession().setAttribute("current_user",adminDB);
            request.getSession().setAttribute("loginTime",new Date());
        }
        return success ? "index" : "login";
    }

    @RequestMapping("/admin/logout")
    public String logout(ModelMap modelMap,HttpServletRequest request) {
        request.getSession().invalidate();//销毁session
        return "login";
    }

    @RequestMapping("/admin/regist")
    public String regist(ModelMap modelMap, Admin admin, HttpServletRequest request) {

        Admin adminDB = adminService.findAdminByLoginname(admin.getLoginname());
        boolean success = true;
        String failMsg = null;
        if (adminDB == null) {
            success = true;
            failMsg = "注册成功，请登录";
        } else {
            success = false;
            failMsg = "用户名已存在";
        }
        modelMap.put("content", "main");
        modelMap.put("fragment","content2");
        modelMap.put("failMsg", failMsg);
        if(success){
            admin.setAdmintype(0);
            String id = UUID.randomUUID().toString().replace("-", "");
            admin.setId(id);
            adminService.insertAdmin(admin);

        }
        return success ? "login" : "regist";
    }
}
