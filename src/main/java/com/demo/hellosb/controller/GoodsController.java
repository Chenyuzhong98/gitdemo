package com.demo.hellosb.controller;

import com.demo.hellosb.common.domain.Admin;
import com.demo.hellosb.common.domain.Goods;
import com.demo.hellosb.common.utils.Result;
import com.demo.hellosb.service.GoodsService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.UUID;

@Controller
public class GoodsController {
    //
    @Autowired
    private GoodsService goodsService;
    @RequestMapping("/goods/list")
    public String toGoodsListPage(ModelMap modelMap){
            modelMap.put("content","goods-list");
            return "index";
    }
    @RequestMapping("/getgoods")
    @ResponseBody
    public Object getGoods(@RequestParam(defaultValue = "1") int pageNo,@RequestParam(defaultValue = "10") int pageSize){
        PageInfo<Goods> pageInfo = goodsService.findGoods(pageNo,pageSize);
        return Result.success(pageInfo);
    }

    @DeleteMapping("/deleteGoods")
    @ResponseBody
    public Object delGood(@RequestBody Goods goods){
        goodsService.deleteGoodsById(goods.getId());
        return Result.success("删除成功");
    }

    @DeleteMapping("/deleteGoodss")
    @ResponseBody
    public Object delGoodss(@RequestBody String[] ids){
            goodsService.deleteGoodsById(ids);
            return  Result.success("批量删除成功");
    }

    @GetMapping("/toupdategoods")
    public String toUpdatePage(ModelMap modelMap,String id){
            Goods goods = goodsService.findGoodsById(id);
            modelMap.put("goods",goods);
            modelMap.put("content","updategoods");
            return "index";
    }

    @RequestMapping("/goods/creatpage")
    public String tocreatpage(ModelMap modelMap){
        modelMap.put("content","creategoods");
        return "index";
    }

    @RequestMapping("/goods/creatgoods")
    public String regist(ModelMap modelMap, Goods goods, HttpServletRequest request) {

        String failMsg = "创建成功";
        String id = UUID.randomUUID().toString().replace("-", "");
        goods.setId(id);
        goods.setGoodssn(request.getParameter("goodsSn"));
        goods.setGoodsname(request.getParameter("goodsName"));
        goods.setGoodsstock(Integer.parseInt(request.getParameter("goodsStock")));
        goods.setCreatorid(Integer.parseInt(request.getParameter("creatid")));


        goodsService.goodsinset(goods);
        modelMap.put("content","creatsuccess");
        modelMap.put("failMsg", failMsg);
        return "index";
    }
    @RequestMapping("/goods/goodsupdate")
    public String update(ModelMap modelMap, Goods goods, HttpServletRequest request) {

        String failMsg = "更改成功";

        Goods goodsInDB = goodsService.findGoodsById(request.getParameter("id"));

        goodsInDB.setGoodssn(request.getParameter("goodsSn"));
        goodsInDB.setGoodsname(request.getParameter("goodsName"));
        goodsInDB.setPrice(Double.parseDouble(request.getParameter("price")));
        goodsInDB.setGoodsstock(Integer.parseInt(request.getParameter("goodsStock")));
        goodsInDB.setCreatorid(Integer.parseInt(request.getParameter("creatid")));
        goodsInDB.setRemark(request.getParameter("remark"));

        goodsService.goodsupdate(goodsInDB);
        modelMap.put("content","updatesuccess");
        modelMap.put("failMsg", failMsg);
        return "index";
    }

}
