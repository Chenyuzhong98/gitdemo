package com.demo.hellosb;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan({"com.demo.hellosb.mapper"})
public class HellosbApplication {

    public static void main(String[] args) {
        SpringApplication.run(HellosbApplication.class, args);
    }

}
