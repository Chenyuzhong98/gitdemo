package com.demo.hellosb.common.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CommonController {
    //定义欢迎页
    @RequestMapping("/")
    public String loginpage(ModelMap modelMap){
//        modelMap.put("content","main");
        return "login";
    }

    //定义欢迎页
    @RequestMapping("/index")
    public String index(ModelMap modelMap){
       modelMap.put("content","main");
        return "index";
    }

    //定义注册页
    @RequestMapping("/regist")
    public String registpage(ModelMap modelMap){
        return "regist";
    }

}
