package com.demo.hellosb.mapper;

import com.demo.hellosb.common.domain.Admin;
import com.demo.hellosb.common.domain.AdminExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AdminMapper {
    long countByExample(AdminExample example);

    int deleteByExample(AdminExample example);

    int deleteByPrimaryKey(String id);

    int insert(Admin record);//常用！

    int insertSelective(Admin record);

    List<Admin> selectByExample(AdminExample example);

    Admin selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") Admin record, @Param("example") AdminExample example);  //常用！

    int updateByExample(@Param("record") Admin record, @Param("example") AdminExample example);

    int updateByPrimaryKeySelective(Admin record);//常用！

    int updateByPrimaryKey(Admin record);
//     扩展一个登陆的方法
    Admin selectAdminByLoginNameAndPassword1(@Param("name") String name,@Param("password")String password);
    Admin selectAdminByLoginNameAndPassword2(@Param("admin")Admin admin);

}